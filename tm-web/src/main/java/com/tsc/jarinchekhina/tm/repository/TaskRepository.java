package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * TaskRepository
 *
 * @author Yuliya Arinchekhina
 */
public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("ONE"));
        add(new Task("TWO"));
        add(new Task("THREE"));
    }

    public void create() {
        add(new Task("New Task " + System.currentTimeMillis()));
    }

    public void add(Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(String id) {
        return tasks.get(id);
    }

    public void removeById(String id) {
        tasks.remove(id);
    }
    
}
