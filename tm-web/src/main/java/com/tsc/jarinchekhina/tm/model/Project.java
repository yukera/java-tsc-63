package com.tsc.jarinchekhina.tm.model;

import com.tsc.jarinchekhina.tm.enumerated.Status;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

/**
 * Project
 *
 * @author Yuliya Arinchekhina
 */
@Data
public class Project {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateStart;

    private Date dateFinish;

    public Project(String name) {
        this.name = name;
    }

}
