package com.tsc.jarinchekhina.tm.servlet;

import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import com.tsc.jarinchekhina.tm.repository.TaskRepository;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * TaskEditServlet
 *
 * @author Yuliya Arinchekhina
 */
@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String projectId = req.getParameter("projectId");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        final String statusValue = req.getParameter("status");
        final Status status = Status.valueOf(statusValue);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateStartValue = req.getParameter("dateStart");
        final String dateFinishValue = req.getParameter("dateStart ");

        final Task task = new Task(name);
        task.setId(id);
        task.setProjectId(projectId);
        task.setDescription(description);
        task.setStatus(status);

        if (!(dateStartValue == null || dateStartValue.isEmpty()))
            task.setDateStart(simpleDateFormat.parse(dateStartValue));
        else
            task.setDateStart(null);

        if (!(dateFinishValue == null || dateFinishValue.isEmpty()))
            task.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else
            task.setDateFinish(null);

        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }
    
}
