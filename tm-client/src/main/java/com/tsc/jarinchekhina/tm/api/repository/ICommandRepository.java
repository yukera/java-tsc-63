package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface ICommandRepository {

    @NotNull
    List<AbstractListener> getCommandList();

    @NotNull
    Collection<AbstractListener> getCommands();

    @NotNull
    Collection<AbstractListener> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArgs();

    @Nullable
    AbstractListener getCommandByName(@NotNull String name);

    @Nullable
    AbstractListener getCommandByArg(@NotNull String name);

    void add(@NotNull AbstractListener command);

}