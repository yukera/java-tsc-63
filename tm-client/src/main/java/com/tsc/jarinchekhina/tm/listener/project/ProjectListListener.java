package com.tsc.jarinchekhina.tm.listener.project;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractProjectListener;
import com.tsc.jarinchekhina.tm.endpoint.ProjectDTO;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectsListNotFoundException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "show project list";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@projectListListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LIST PROJECTS]");
        @Nullable List<ProjectDTO> projects = getProjectEndpoint().findAllProjects(serviceLocator.getSession());
        if (projects == null) throw new ProjectsListNotFoundException();
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final String projectStatus = project.getStatus().value();
            System.out.println(index + ". " + project.getId() + ' ' + project.getName() + " (" + projectStatus + ")");
            index++;
        }
    }

}
