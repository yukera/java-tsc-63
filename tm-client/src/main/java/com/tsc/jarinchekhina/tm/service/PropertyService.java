package com.tsc.jarinchekhina.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

/**
 * PropertyService
 *
 * @author Yuliya Arinchekhina
 */

@Getter
@Service
@PropertySource("classpath:config.properties")
public class PropertyService {

    @Value("${app.version}")
    private String appVersion;

}
