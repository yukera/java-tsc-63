package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.List;

public interface ICommandService {

    @NotNull
    List<AbstractListener> getCommandList();

    @NotNull
    Collection<AbstractListener> getCommands();

    @NotNull
    Collection<AbstractListener> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArgs();

    @Nullable
    AbstractListener getCommandByName(@Nullable String name);

    @Nullable
    AbstractListener getCommandByArg(@Nullable String name);

    void add(@Nullable AbstractListener command);

}
