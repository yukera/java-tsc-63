package com.tsc.jarinchekhina.tm.listener.task;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractTaskListener;
import com.tsc.jarinchekhina.tm.endpoint.Status;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public final class TaskStatusChangeByNameListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-status-change-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "change task status by name";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskStatusChangeByNameListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.fromValue(statusId);
        getTaskEndpoint().changeTaskStatusByName(serviceLocator.getSession(), name, status);
    }

}
