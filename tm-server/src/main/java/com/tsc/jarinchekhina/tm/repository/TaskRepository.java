package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractRepository<Task> {

    @NotNull
    Task save(@NotNull final Task task);

    void deleteAll();

    void deleteAllByUser_Id(@NotNull final String userId);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUser_IdAndProject_Id(@NotNull final String userId, @NotNull final String projectId);

    @NotNull
    List<Task> findAllByUser_Id(@NotNull final String userId);

    void deleteAllByProject_Id(@NotNull final String projectId);

    void delete(@NotNull final Task task);

    void deleteById(@NotNull final String id);

    @NotNull
    Optional<Task> findFirstById(@NotNull final String id);

    void deleteByName(@NotNull final String name);

    @NotNull
    Optional<Task> findFirstByUser_IdAndName(@NotNull final String userId, @NotNull final String name);

}
