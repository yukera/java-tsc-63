package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void add(@Nullable String userId, @Nullable Task task);

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void clear(@Nullable String userId);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void remove(@Nullable String userId, @Nullable Task task);

    void removeAllByProjectId(@NotNull String projectId);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    Task findById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task findByName(@Nullable String userId, @Nullable String name);

    void updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void startTaskById(@Nullable String userId, @Nullable String id);

    void startTaskByName(@Nullable String userId, @Nullable String name);

    void finishTaskById(@Nullable String userId, @Nullable String id);

    void finishTaskByName(@Nullable String userId, @Nullable String name);

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void changeTaskStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
